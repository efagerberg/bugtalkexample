﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Vector3 m_velocity;
    [SerializeField]
    private Rigidbody m_rb;

    private void Update() { HandleMovement(); }
    private void FixedUpdate() { PerformMovement(); }
    private void HandleMovement()
    {
        float _xMov = Input.GetAxis("Horizontal");
        float _zMov = Input.GetAxis("Vertical");
        Vector3 _movHorizontal = transform.right * _xMov;
        Vector3 _movVertical = transform.forward * _zMov;
        m_velocity = (_movHorizontal + _movVertical).normalized * 5f;
    }

    private void PerformMovement()
    {
        m_rb.MovePosition(m_rb.position + m_velocity * Time.fixedDeltaTime);
    }
}