﻿using UnityEngine;

public class HumblePlayerController {
    private Vector3 m_velocity;
    public Vector3 Velocity { get { return m_velocity; } }

    public void HandleMovement(Transform _transform, float _xMov, float _zMov)
    {
        Vector3 _movHorizontal = _transform.right * _xMov;
        Vector3 _movVertical = _transform.forward * _zMov;
        m_velocity = (_movHorizontal + _movVertical).normalized * 5f;
    }
}
