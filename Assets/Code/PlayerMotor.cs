﻿using UnityEngine;

public class PlayerMotor : MonoBehaviour {
    [SerializeField]
    private Rigidbody m_rb;
    private HumblePlayerController m_playerController = new HumblePlayerController();

    private void FixedUpdate()
    {
        m_rb.MovePosition(m_rb.position  + m_playerController.Velocity * Time.fixedDeltaTime);
    }

    private void Update()
    {
        m_playerController.HandleMovement(transform, Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }
}
