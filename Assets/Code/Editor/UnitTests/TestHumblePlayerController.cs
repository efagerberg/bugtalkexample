﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class TestHumblePlayerController
{
    private HumblePlayerController pControllerUnderTest;
    private Transform transform;
    private GameObject go;

    [SetUp]
    public void Init()
    {
        pControllerUnderTest = new HumblePlayerController();
        go = new GameObject("TestGO");
        transform = go.transform;
    }

    [TearDown]
    public void Dispose()
    {
        pControllerUnderTest = null;
        transform = null;
        go = null;
    }

    [TestCase(0, 0)]
    [TestCase(0, 1)]
    [TestCase(1, 0)]
    [TestCase(1, 1)]
    [TestCase(0, -1)]
    [TestCase(-1, 0)]
    [TestCase(-1, -1)]
    public void HandleMovement_CalculatesExpectedVelocity(float inputX, float inputZ)
    {
        var expectedVelocity = new Vector3(inputX, 0, inputZ).normalized * 5;
        pControllerUnderTest.HandleMovement(transform, inputX, inputZ);
        Assert.AreEqual(pControllerUnderTest.Velocity, expectedVelocity);
    }
}
